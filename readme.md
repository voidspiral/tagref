# TagRef2

*An application for displaying reference images, imspired by GestureDrawing and TagSpaces*

# Features

* Bring *your own* curated reference images from any source
* Timed image display shows reference images in a random order for you to draw from, and includes next, previous, and pause functionality
* Filter images only containing specific tags, block images with specific tags, or even blacklist specific images when they come up
* 

## Roadmap

* Autosave session file on change
* File Logging: keep track of which images you just used
* Structured Mode: set up runs of images of whatever time you wish, and intersperse timed breaks
* File Tagger: Tag images directly in the app
* Drawing Stats: Track what tags you've used, how much you've drawn, and more

# Development

* Package Manager: [`pipenv`](https://pipenv.pypa.io/en/latest/)
	* Set up dev environment: `pipenv install`
	* Run app in dev mode: `pipenv run python tagref.py`
	* Locate virtual environment: `pipenv --venv`
	* Dependencies: `pipenv graph`
	* Build (windows): `pipenv run python ./build.py`
	* Security: `pipenv check`

## Python Packages

* Pillow
* QDarkStyle
* PyQt5
* PyQt5-sip
* PyQt5-tools
* appdirs
* PyInstaller

## Other Requirements

* Qt(?)
* Qt Designer
* a Python IDE