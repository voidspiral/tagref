from PyQt5 import QtWidgets, QtCore, uic, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
import sys, os, glob, configparser, subprocess, platform
from pathlib import Path

class Tagger():
	def __init__(self, app):
		self.app = app
		self.rootdir = ""
		self.items = []
		self.app.actionTag_Files.triggered.connect(self.GoTo)
		self.app.actionExit_Tagging.triggered.connect(self.Leave)
		self.app.actionExit_Tagging.setEnabled(False)
		self.tree = self.app.treeTaggerDir
		self.tree.setIndentation(20)
		self.tree.setRootIsDecorated(True)
		self.tree.setItemsExpandable(True)
		self.tree.header().hide()
		self.tree.clicked.connect(self.TreeItemActivated)
		self.path = ""
		self.app.listTaggerImages.itemSelectionChanged.connect(self.SetSelection)
		self.app.listTaggerImages.doubleClicked.connect(self.OpenImage)
		self.app.buttonTaggerRenameFiles.clicked.connect(self.SetFileName)
		self.app.buttonTaggerAddNewTag.clicked.connect(self.AddNewTag)
		self.app.listTaggerProjectTags.doubleClicked.connect(self.AddProjectTag)
		self.app.buttonTaggerAddProjectTag.clicked.connect(self.AddProjectTag)
		self.app.buttonTaggerRemoveTag.clicked.connect(self.RemoveSelectionTag)
		self.app.buttonTaggerReplaceTag.clicked.connect(self.ReplaceSelectionTag)
		# toggle widgets based on selection
		self.selectionRequired = [
			self.app.lineTaggerEditNewTag,
			self.app.buttonTaggerAddNewTag,
			self.app.lineTaggerEditName,
			self.app.buttonTaggerRenameFiles,
			self.app.buttonTaggerRemoveTag,
			self.app.buttonTaggerReplaceTag
		]
		self.ToggleSelectionItems()

	def GoTo(self):
		self.app.pages.setCurrentIndex(2)
		self.app.actionTag_Files.setEnabled(False)
		self.app.actionExit_Tagging.setEnabled(True)
		self.SetRoot()

	def SetRoot(self):
		self.rootdir = self.app.labelSettingsDir.text()
		print(self.rootdir)
		self.path = self.rootdir
		# QtCore.QDir.setCurrent(self.rootdir)
		model = QtWidgets.QFileSystemModel()
		model.filePath(model.parent(model.index(self.path)))
		model.setRootPath(self.rootdir)
		filters = QtCore.QDir.AllDirs | QtCore.QDir.NoDotAndDotDot
		model.setFilter(filters)
		model.setNameFilterDisables(False)
		self.tree.setModel(model)
		# self.tree.setRootIndex(model.index(self.rootdir))
		self.tree.setRootIndex(model.parent(model.index(self.path)))
		self.tree.setColumnHidden(1, True)
		self.tree.setColumnHidden(2, True)
		self.tree.setColumnHidden(3, True)
		self.tree.setRootIsDecorated(True)
		self.tree.expandRecursively(model.index(self.path), -1)
		self.GetProjectTags()
		self.GetFolderContents(self.rootdir)

	def TreeItemActivated(self, index):
		index = self.tree.selectedIndexes()[0]
		# item = index.model().itemFromIndex(index)
		# print(item)
		self.path = self.tree.model().filePath(index)
		print(self.path)
		self.GetFolderContents(self.path)

	def GetFolderContents(self, dirpath):
		self.app.listTaggerImages.clear()
		# get filenames in dirpath
		self.pathFiles = []
		for extension in self.app.extensions:
			temp = glob.glob(dirpath + "/" + extension, recursive=False)
			self.pathFiles.extend(temp)
		# button for each
		for item in self.pathFiles:
			# print(item.encode("utf-8"))
			self.AddItem(item, item)

	def AddItem(self, path, text):
		view = self.app.listTaggerImages
		basename = os.path.basename(path)
		# basename = basename[:20]
		tags = {}
		self.GetTagsFromPath(path, tags)
		# try to add a button for the tags
		item = QtWidgets.QListWidgetItem(QtGui.QIcon(path), basename)
		view.addItem(item)

	def SetSelection(self):
		listWidget = self.app.listTaggerImages
		model = listWidget.selectionModel()
		self.items = []
		for item in listWidget.selectedItems():
			# print(item.text())
			self.items.append(item)
		self.GetSelectionTags()
		self.ToggleSelectionItems()

	def GetFirstSelection(self, items):
		if len(items) > 0:
			self.firstItem = items[0]
			nameonly = self.GetFileNameOnly(self.firstItem.text())
			self.app.lineTaggerEditName.setText(nameonly)
		else:
			self.firstItem = None

	def SetFileName(self):
		directory = self.path
		changedValue = self.app.lineTaggerEditName.text()
		# get a list of all existing filenames starting with changedvalue
		# self.pathFiles
		# get a list of all file name chunks
		titles = list(map(lambda x: self.GetFileNameOnly(os.path.basename(x)), self.pathFiles))
		index = 0
		if changedValue != '':
			for item in self.items:
				originalValue = item.text()
				reconstructed = changedValue
				while reconstructed in titles:
					index += 1
					reconstructed = changedValue + ' ' + str(index)
				reconstructed += self.PassTagsThrough(originalValue)
				reconstructed += self.PassExtThrough(originalValue)
				try:
					os.rename(originalValue, reconstructed)
					item.setText(reconstructed)
					print('renamed file ' + originalValue + ' > '  + reconstructed)
				except:
					print('could not write file ' + reconstructed)
				
				index += 1
		else:
			print('filename was blank\a')

	def Leave(self):
		self.app.pages.setCurrentIndex(0)
		self.app.actionTag_Files.setEnabled(True)
		self.app.actionExit_Tagging.setEnabled(False)

	def GetProjectTags(self):
		self.projectTags = {}
		self.GetTagsFromFiles(self.app.session.allFiles, self.projectTags)
		# print(self.projectTags.encode("utf-8"))
		self.GenerateTagCloud(self.projectTags, self.app.listTaggerProjectTags)
	
	def GetSelectionTags(self):
		if len(self.items) > 0:
			self.GetFirstSelection(self.items)
			itemPaths = map(lambda x : x.text(), self.items)
			# self.GetSelectionTags(itemPaths)
			self.selectionTags = {}
			self.GetTagsFromFiles(itemPaths, self.selectionTags)
			self.GenerateTagCloud(self.selectionTags, self.app.listTaggerSelectionTags)
			# print(self.selectionTags)

	def GenerateTagCloud(self, dictionary, listWidget):
		# remove old items
		listWidget.clear()
		# add new items
		for key in sorted(dictionary):
			listWidget.addItem(key)

	def GetTagsFromFiles(self, paths, dictionary):
		for p in paths:
			self.GetTagsFromPath(p, dictionary)

	def GetTagsFromPath(self, p, dictionary):
		if '[' in p and ']' in p:
			tagchunk = p.split('[')[1].split(']')[0]
			split = tagchunk.split(' ')
			for tag in split:
				if tag in dictionary:
					dictionary[tag].append(p)
				else:
					dictionary[tag] = [p]

	def RecompileTagList(self, tags):
		if len(tags) > 0:
			output = ' ['
			for tag in tags:
				output += tag
				output += ' '
			output = output.strip()
			output += ']'
			return output
		else:
			return ''

	def GetFileNameOnly(self, p):
		if '[' in p and ']' in p:
			namechunk = p.split('[')[0]
		else:
			namechunk = p.split('.')[0]
		namechunk = namechunk.strip()
		return namechunk

	def PassTagsThrough(self, p):
		if '[' in p and ']' in p:
			tagchunk = p.split('[')[1].split(']')[0]
			tagchunk = ' [' + tagchunk + ']'
			return tagchunk
		else:
			return ''

	def PassExtThrough(self, p):
		split = p.split('.')
		return '.' + split[-1]

	def AddNewTag(self):
		tag = self.app.lineTaggerEditNewTag.text()
		self.AddTagToSelection(tag)

	def AddTagToSelection(self, tag):
		if len(self.items) > 0:
			for item in self.items:
				self.AddTagToItem(item, tag)

	def AddTagToItem(self, item, tag):
		original = item.text()
		# print(original + tag)
		# decompose into name, taglist, and ext
		filename = self.GetFileNameOnly(original)
		taglist = {}
		self.GetTagsFromPath(original, taglist)
		ext = self.PassExtThrough(original)
		# add tag to list
		taglist[tag] = original
		# recompose
		final = filename + self.RecompileTagList(taglist.keys()) + ext
		# rename file
		try:
			os.rename(os.path.join(self.path, original), os.path.join(self.path,final))
			item.setText(final)
			print('Renamed ' + original + ' ' + final)
		except Exception as e:
			print('could not rename file: ' + original + ' ' + repr(e))
		if tag in self.projectTags:
			self.projectTags[tag].append(final)
		else:
			self.projectTags[tag] = [final]
		self.GenerateTagCloud(self.projectTags, self.app.listTaggerProjectTags)

	def RemoveSelectionTag(self):
		tag = self.app.listTaggerSelectionTags.currentItem().text()
		if tag:
			self.RemoveTagFromSelection(tag)

	def RemoveTagFromSelection(self, tag):
		if len(self.items) > 0:
			for item in self.items:
				self.RemoveTagFromItem(item, tag)

	def RemoveTagFromItem(self, item, tag):
		original = item.text()
		filename = self.GetFileNameOnly(original)
		taglist = {}
		self.GetTagsFromPath(original, taglist)
		ext = self.PassExtThrough(original)
		try:
			del taglist[tag]
			final = filename + self.RecompileTagList(taglist.keys()) + ext
			try:
				os.rename(original, final)
				item.setText(final)
			except:
				print('could not rename file: ' + original)
		except:
			print('file did not have tag ' + tag)
		

	def AddProjectTag(self):
		tag = self.app.listTaggerProjectTags.currentItem().text()
		self.AddTagToSelection(tag)

	def ReplaceSelectionTag(self):
		currentItem = self.app.listTaggerSelectionTags.currentItem()
		if currentItem:
			# get the selected tag
			oldTag = currentItem.text()
			# get the new tag text
			newTag = self.app.lineTaggerEditNewTag.text()
			# iterate over items
			if oldTag and newTag:
				for item in self.items:
					self.ReplaceTag(item, oldTag, newTag)

	def ReplaceTag(self, item, oldTag, newTag):
		original = item.text()
		filename = self.GetFileNameOnly(original)
		taglist = {}
		self.GetTagsFromPath(original, taglist)
		ext = self.PassExtThrough(original)
		try:
			del taglist[oldTag]
			taglist[newTag] = [original]
			final = filename + self.RecompileTagList(taglist.keys()) + ext
			try:
				os.rename(original, final)
				item.setText(final)
			except:
				print('could not rename file: ' + original)
		except:
			print('file did not have tag ' + tag)
		self.GetSelectionTags()

	def ToggleSelectionItems(self):
		if len(self.items) > 0:
			enabled = True
		else:
			enabled = False
		for widget in self.selectionRequired:
			widget.setEnabled(enabled)

	def OpenImage(self):
		listWidget = self.app.listTaggerImages
		filename = listWidget.currentItem().text()
		clickedImagePath = os.path.join(self.path, filename)
		# print(clickedImagePath)
		if platform.system() == 'Darwin':
			subprocess.call(('open', clickedImagePath))
		elif platform.system() == 'Windows':
			os.startfile(clickedImagePath)
		else:
			subprocess.call(['xdg-open', clickedImagePath])