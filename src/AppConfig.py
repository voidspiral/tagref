from PyQt5 import QtWidgets, QtCore, uic, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
import sys, os, glob, configparser
from pathlib import Path

class AppConfig():
	def __init__(self, app):
		self.app = app

	def SetAppConfig(self, path):
		self.app.appcp['DEFAULT']['lastconfigfile'] = path
		Path(self.app.userdatadir).mkdir(parents=True, exist_ok=True)
		with open(self.app.persistentdata, 'w') as cf:
			self.app.appcp.write(cf)

	def GetAppConfig(self):
		self.app.appcp.read(self.app.persistentdata)
		if 'DEFAULT' in self.app.appcp:
			if 'lastconfigfile' in self.app.appcp['DEFAULT']:
				lastconfigfile = self.app.appcp['DEFAULT']['lastconfigfile']
				print('last config: ' + lastconfigfile)
				self.app.fileops.configfile = lastconfigfile
				self.app.fileops.LoadConfig()