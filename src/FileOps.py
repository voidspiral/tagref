from PyQt5 import QtWidgets, QtCore, uic, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
import sys, os, glob, configparser
from pathlib import Path

class FileOps():
	def __init__(self, app):
		self.app = app
		self.configfile = ""
		self.cansave = True
		self.configparser = configparser.ConfigParser()
		a = self.app
		self.fields = [
			a.labelSettingsDir,	
			a.textSettingsSearch,
			a.textSettingsBlacklist,
			a.textSettingsBlock,
			a.tabSettingsSessionMode,
			a.layoutSettingsFixedTime,
			a.spinSettingsFixedNumber,
			a.spinSettingsFixedCustomSec,
			a.spinSettingsFixedCustomMin,
			a.comboSettingsHorizontal,
			a.comboSettingsVertical,
			a.checkSettingsBw,
			a.checkSettingsMute,
			a.toggleSessionMute,
			a.sliderSettingsVolume,
			a.checkSettingsLog,
			a.checkSettingsLiveLog,
			a.labelSettingsLogPath,
			a.labelSettingsLivePath,
			a.classTypeRadios,
			a.fixedLengthRadios,
		]

	def SetupMenu(self):
		# setup connections to the menu actions
		self.app.actionSave_Session_Settings.triggered.connect(self.SaveConfig)
		self.app.actionSave_Session_Settings_As.triggered.connect(self.SaveAsConfig)
		self.app.actionLoad_Session_Settings.triggered.connect(self.LoadConfig)

	def ShowLoadDialog(self):
		print('Show load dialog')
		self.LoadConfig()

	def LoadConfig(self):
		# self.ShowLoadDialog()
		print('load config: ' + self.configfile)
		self.configparser.read(self.configfile)
		self.cansave = False
		for field in self.fields:
			self.GetFieldFromParser(field)
		self.cansave = True
		self.app.session.GetFileList()

	def SaveAsConfig(self):
		print('save as config')
		self.ShowSaveDialog()
		self.SaveConfig()

	def SaveConfig(self):
		if self.cansave:
			print('save config')
			if self.configfile == '':
				# show save dialog
				self.ShowSaveDialog()
			if not os.path.isfile(self.configfile):
				self.ShowSaveDialog()
			# set all variables
			for field in self.fields:
				self.SetParserField(field)
			# Save config file
			with open(self.configfile, 'w+') as cf:
				cf.truncate(0)
				self.configparser.write(cf)
			# update last used file
			self.app.appconfig.SetAppConfig(self.configfile)

	def SaveIfPossible(self):
		if self.configfile:
			self.SaveConfig()

	def SetParserField(self, field):
		d = self.configparser['DEFAULT']
		a = self.app
		fieldname = field.objectName()
		if fieldname == '':
			print('field without object name: ' + field)
		if isinstance(field, QtWidgets.QPlainTextEdit):
			d[fieldname] = field.toPlainText()
		elif isinstance(field, QtWidgets.QLabel):
			d[fieldname] = field.text()
		elif isinstance(field, QtWidgets.QButtonGroup):
			d[fieldname] = str(field.checkedId())
		elif isinstance(field, QtWidgets.QTabWidget):
			d[fieldname] = str(field.currentIndex())
		elif isinstance(field, QtWidgets.QSpinBox):
			d[fieldname] = str(field.value())
		elif isinstance(field, QtWidgets.QComboBox):
			d[fieldname] = str(field.currentIndex())
		elif isinstance(field, QtWidgets.QCheckBox):
			d[fieldname] = str(field.isChecked())
		elif isinstance(field, QtWidgets.QSlider):
			d[fieldname] = str(field.value())

	def GetFieldFromParser(self, field):
		d = self.configparser['DEFAULT']
		a = self.app
		fieldname = field.objectName()
		if fieldname in d:
			value = d[fieldname]
			# print(fieldname + ': ' + value)
			if isinstance(field, QtWidgets.QPlainTextEdit):
				field.setPlainText(value)
			elif isinstance(field, QtWidgets.QLabel):
				field.setText(value)
			elif isinstance(field, QtWidgets.QButtonGroup):
				for button in field.buttons():
					if field.id(button) == int(value):
						button.setChecked(True)
			elif isinstance(field, QtWidgets.QTabWidget):
				field.setCurrentIndex(int(value))
			elif isinstance(field, QtWidgets.QSpinBox):
				field.setValue(float(value))
			elif isinstance(field, QtWidgets.QComboBox):
				field.setCurrentIndex(int(value))
			elif isinstance(field, QtWidgets.QCheckBox):
				field.setChecked(value == 'True')
			elif isinstance(field, QtWidgets.QSlider):
				field.setValue(float(value))

	def ShowSaveDialog(self):
		dialog = QFileDialog(self.app)
		# dialog.setFileMode(QFileDialog.AnyFile)
		dialog.setNameFilters(['Config Files (*.ini)'])
		dialog.selectNameFilter('Config Files (*.ini)')
		filename, dummy = dialog.getSaveFileName(self.app, 'Save File')
		print(filename)
		self.configfile = filename