from PyQt5 import QtWidgets, QtCore, uic, QtGui, QtMultimedia
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
import sys, os, glob, configparser, random
from PIL import Image, ImageQt

class Session():
	def __init__(self, app):
		self.app = app
		self.timer = QtCore.QTimer()
		self.timer.timeout.connect(self.TimerTick)
		self.seconds = 0
		self.imageLength = 0
		boopfile = self.app.Resource('boop.wav')
		self.boop = QtMultimedia.QSoundEffect()
		self.boop.setSource(QtCore.QUrl.fromLocalFile(boopfile))
		beepfile = self.app.Resource('beep.wav')
		self.beep = QtMultimedia.QSoundEffect()
		self.beep.setSource(QtCore.QUrl.fromLocalFile(beepfile))
		self.allFiles = []
		self.app.tabSettingsSessionMode.setTabEnabled(1, False)
		self.app.tabSettingsSessionMode.setTabEnabled(2, False)
		self.app.tabSettingsSessionMode.setTabEnabled(3, False)

	def ChooseDir(self):
		directory = QFileDialog.getExistingDirectory(self.app, "Select Directory")
		if directory:
			self.app.labelSettingsDir.setText(directory)
			print('set new session directory: ' + directory)
			self.GetFileList()
			self.app.fileops.SaveIfPossible()

	def GetFileList(self):
		f = []
		for extension in self.app.extensions:
			temp = glob.glob(self.app.labelSettingsDir.text() + "/**/" + extension, recursive=True)
			f.extend(temp)
		self.app.labelSettingsDirCount.setText(str(len(f)) + " files")
		f = self.FilterWhitelist(f)
		f = self.FilterBlacklist(f)
		f = self.FilterBlocklist(f)
		self.allFiles = f
		self.app.labelSettingsFileCount.setText('File Count: ' + str(len(self.allFiles)))
		self.CalculateSessionTime()

	def LimitList(self, f):
		tabIndex = self.app.tabSettingsSessionMode.currentIndex()
		sessionType = self.app.tabSettingsSessionMode.tabText(tabIndex)
		classType = self.app.classTypeRadios.checkedButton()
		print(sessionType)
		if sessionType == 'Fixed Length':
			f = f[0:self.app.spinSettingsFixedNumber.value()]
		elif sessionType == 'Class Session':
			pass
		return f

	def FilterWhitelist(self, items):
		whitelist = self.app.textSettingsSearch.toPlainText()
		return self.Filter(items, whitelist, True)

	def FilterBlocklist(self, items):
		blocklist = self.app.textSettingsBlock.toPlainText()
		return self.Filter(items, blocklist, False)

	def FilterBlacklist(self, items):
		blacklist = self.app.textSettingsBlacklist.toPlainText()
		return self.Filter(items, blacklist, False)

	def Filter(self, items, filterstring, keep):
		# return whole list if the filter is empty
		if filterstring.isspace() or filterstring == '':
			return items
		# setup for keep list
		outlist = []
		terms = filterstring.split('\n')
		# if item matches term, perform keep
		for item in items:
			added = False
			for term in terms:
				if not (term.isspace() or term == ''):
					if (term.lower() in item.lower()) == keep:
						if added == False:
							outlist.append(item)
							added = True
		return outlist

	def CalculateSessionTime(self):
		# get image length
		text = self.app.fixedLengthRadios.checkedButton().text()
		# print(text)
		time = 0
		if text == 'custom seconds':
			time = self.app.spinSettingsFixedCustomSec.value()
		elif text == 'custom minutes':
			time = self.app.spinSettingsFixedCustomMin.value() * 60
		else:
			increment = text[-1]
			value = int(text.replace(increment, ''))
			if increment == 'm':
				value *= 60
			time = value
		self.imageLength = time
		# mutliply by image count
		count = self.app.spinSettingsFixedNumber.value()
		totaltime = count * time
		# set time label
		f = self.app.FormatTime(totaltime)
		self.app.labelSettingsSessionTime.setText('Session Time: ' + f)
		# self.app.fileops.SaveIfPossible()
		if time == 0 or len(self.allFiles) == 0:
			self.app.buttonSettingsStart.setEnabled(False)
		else:
			self.app.buttonSettingsStart.setEnabled(True)
		
	def StartSession(self):
		# setup UI
		self.app.layoutSessionEndscreen.hide()
		self.app.layoutSessionToolbar.show()
		self.app.viewer.show()
		self.app.buttonSessionPause.setChecked(False)
		# set volume
		vol = self.app.sliderSettingsVolume.value() / 100.0
		a = QtMultimedia.QAudio.LogarithmicVolumeScale
		b = QtMultimedia.QAudio.LinearVolumeScale
		vol = QtMultimedia.QAudio.convertVolume(vol, a, b)
		self.beep.setVolume(vol)
		self.boop.setVolume(vol)
		# manipulate files
		self.sessionFiles = self.allFiles
		# Shuffle
		random.shuffle(self.sessionFiles)
		# Limit
		self.sessionFiles = self.LimitList(self.sessionFiles)
		# Start first image
		self.app.pages.setCurrentIndex(1)
		self.index = 0
		self.SetImage(self.index)
		self.seconds = self.imageLength
		self.timer.start(1000)

	def NextImage(self):
		# Limit index
		mi = len(self.sessionFiles) - 1
		self.index = self.index + 1
		# self.index = min(mi, self.index)
		if self.index > mi:
			self.SessionComplete()
		else:
			self.SetImage(self.index)
			self.seconds = self.imageLength
			self.timer.start(1000)

	def PreviousImage(self):
		# Limit index
		mi = 0
		self.index = self.index - 1
		self.index = max(mi, self.index)
		# print("prev: " + str(self.index))
		self.SetImage(self.index)
		self.seconds = self.imageLength
		self.timer.start(1000)

	def TimerTick(self):
		self.timer.stop()
		timertext = self.app.FormatTime(self.seconds)
		self.app.labelSessionTime.setText(timertext)
		# print(timertext)
		if self.seconds <= 0:
			# todo: set time from settings
			self.seconds = self.imageLength
			if not self.app.toggleSessionMute.isChecked():
				self.beep.play()
			self.NextImage()
		elif self.seconds < 4:
			if not self.app.toggleSessionMute.isChecked():
				self.boop.play()
		self.seconds -= 1
		if self.app.layoutSessionEndscreen.isHidden():
			self.timer.start(1000)

	def PauseSession(self):
		widget = self.app.buttonSessionPause
		if widget.isChecked() == True:
			self.timer.stop()
		else:
			self.timer.start(1000)
		
	def SetImage(self, index):
		path = self.sessionFiles[index]
		img = QtGui.QPixmap(path)
		# do grayscale conversion if needed
		if self.app.checkSettingsBw.isChecked():
			original = Image.open(path)
			greyscale = original.convert(mode="L")
			qimage = ImageQt.ImageQt(greyscale)
			img = QtGui.QPixmap.fromImage(qimage)
		# do image flipping if needed
		transform = QtGui.QTransform()
		# horizontal flippint
		fliphstate = self.app.comboSettingsHorizontal.currentIndex()
		fliphr = random.choice([1,-1])
		if fliphstate == 0:
			fliph = 1
		elif fliphstate == 1:
			fliph = fliphr
		else:
			fliph = -1
		# vertical flipping
		flipvstate = self.app.comboSettingsVertical.currentIndex()
		flipvr = random.choice([1,-1])
		if flipvstate == 0:
			flipv = 1
		elif flipvstate == 1:
			flipv = flipvr
		else:
			flipv = -1
		# Apply flip
		transform.scale(fliph, flipv)
		img = img.transformed(transform)
		# set the image viewer
		self.app.viewer.setImage(img)
		completion = str(self.index + 1) + '/' + str(len(self.sessionFiles))
		self.app.labelSessionCompletion.setText(completion)

	def MuteFromSettings(self):
		isMuted = self.app.checkSettingsMute.isChecked()
		self.app.toggleSessionMute.setChecked(isMuted)

	def MuteFromSession(self):
		isMuted = self.app.toggleSessionMute.isChecked()
		self.app.checkSettingsMute.setChecked(isMuted)

	def BlacklistImage(self):
		# first, get the index & path
		index = self.index
		path = self.allFiles[index]
		# now remove that image from the list
		del self.allFiles[index]
		# add the image to the blacklist
		textbox = self.app.textSettingsBlacklist
		text = textbox.toPlainText()
		text = text + '\n' + path
		textbox.setPlainText(text)
		# save settings
		self.app.fileops.SaveIfPossible()
		# now move next if possible, else move prev
		if self.index == len(self.allFiles) - 1:
			# I'm last, do prev
			self.PreviousImage()
		else:
			# I'm not last, do next
			self.NextImage()

	def SessionComplete(self):
		self.timer.stop()
		# self.PauseSession()
		self.app.layoutSessionToolbar.hide()
		self.app.layoutSessionEndscreen.show()
		self.app.viewer.hide()
		text = "\n".join(self.sessionFiles)
		self.app.browserSessionInfo.setText(text)

	def EndSession(self):
		self.app.pages.setCurrentIndex(0)
		self.timer.stop()