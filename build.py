import os, shutil, platform
import PyInstaller.__main__

# https://pyinstaller.readthedocs.io/en/stable/usage.html
winicon = '--icon=%s' % os.path.join('resources', 'icon.ico')
macicon = '--icon=%s' % os.path.join('resources', 'icon.icns')
icon = winicon if platform.system() == 'Windows' else macicon
packagename = 'TagRef'
PyInstaller.__main__.run([
	'--name=%s' % packagename,
	'--clean',
	'--noconfirm',
	# '--debug=all',
	'--add-data=resources' + os.pathsep + 'resources',
	'--onefile',
	'--windowed',
	icon,
	'TagRef.py'
	])

# print('copying resource files')
# src = 'resources'
# dest = 'dist/tagref/resources'
# if os.path.exists(dest):
# 	shutil.rmtree(dest)
# shutil.copytree(src, dest)
# print('build complete')