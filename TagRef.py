from PyQt5 import QtWidgets, QtCore, uic, QtGui
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
from PyQt5.QtGui import QPainter

import sys, os, glob, configparser
from pathlib import Path

# custom modules
from src import FileOps, AppConfig, Session, Tagger
from src.QtImageViewer import QtImageViewer
# import FileOps, AppConfig, Session
# from QtImageViewer import QtImageViewer
from appdirs import *

import qdarkstyle

# setup main window ui from designer file
try:
	wd = sys._MEIPASS
except AttributeError:
	wd = os.getcwd()
uifile = os.path.join(wd, 'resources', 'main.ui')
Ui_MainWindow, QtBaseClass = uic.loadUiType(uifile)

class TagRef(QMainWindow, Ui_MainWindow):
	def __init__(self):
		QMainWindow.__init__(self)
		Ui_MainWindow.__init__(self)
		self.setupUi(self)
		self.setWindowTitle('TagRef')
		# dummy ui elements
		path = self.Resource('blank.jpg')
		self.blankImage = QtGui.QImage(path)
		self.extensions = [
			'*.jpg',
			'*.png',
			'*.svg',
			'*.tga',
			'*.jpeg',
			'*.tif',
			'*.tiff',
			'*.webp'
		]
		# setup class structure
		self.CreateProgrammaticElements()
		self.fileops = FileOps.FileOps(self)
		self.fileops.SetupMenu()
		self.appconfig = AppConfig.AppConfig(self)
		self.session = Session.Session(self)
		self.tagger = Tagger.Tagger(self)
		# timer for re-checking the filters
		self.refreshtimer = QtCore.QTimer()
		self.refreshtimer.timeout.connect(self.EndRefreshTimer)
		self.needsrefresh = True
		# Setup app data
		self.SetupButtonCallbacks()
		self.userdatadir = user_data_dir('TagRef', 'Voidspiral')
		self.persistentdata = self.userdatadir + '\\data.ini'
		self.appcp = configparser.ConfigParser()
		# setup ui
		self.SetupViewer()
		self.pages.setCurrentIndex(0)
		# Get last config file and load it
		self.appconfig.GetAppConfig()

	def CreateProgrammaticElements(self):
		self.fixedLengthRadios = QtWidgets.QButtonGroup()
		self.fixedLengthRadios.setObjectName('fixedLengthRadios')
		layout = self.layoutSettingsFixedTime
		flradios = (layout.itemAt(i) for i in range(layout.count()))
		for item in flradios:
			widget = item.widget()
			widgetType = type(widget)
			if isinstance(widget, QtWidgets.QRadioButton):
				self.fixedLengthRadios.addButton(widget)
		self.classTypeRadios = QtWidgets.QButtonGroup()
		self.classTypeRadios.setObjectName('classTypeRadios')
		layout2 = self.gridSettingsClass
		ctradios = (layout2.itemAt(i) for i in range(layout2.count()))
		for item in ctradios:
			widget = item.widget()
			widgetType = type(widget)
			if isinstance(widget, QtWidgets.QRadioButton):
				self.classTypeRadios.addButton(widget)	

	def SetupButtonCallbacks(self):
		# Settings page
		self.buttonSettingsChooseDir.clicked.connect(self.session.ChooseDir)		
		self.fixedLengthRadios.buttonToggled.connect(self.session.CalculateSessionTime)
		self.spinSettingsFixedCustomSec.valueChanged.connect(self.session.CalculateSessionTime)
		self.spinSettingsFixedCustomMin.valueChanged.connect(self.session.CalculateSessionTime)
		self.spinSettingsFixedNumber.valueChanged.connect(self.session.CalculateSessionTime)
		self.textSettingsSearch.textChanged.connect(self.StartRefreshTimer)
		self.textSettingsBlock.textChanged.connect(self.StartRefreshTimer)
		self.textSettingsBlacklist.textChanged.connect(self.StartRefreshTimer)
		self.buttonSettingsStart.clicked.connect(self.session.StartSession)

		# Session page
		self.buttonSessionEnd.clicked.connect(self.session.EndSession)
		self.buttonSessionNext.clicked.connect(self.session.NextImage)
		self.buttonSessionPrevious.clicked.connect(self.session.PreviousImage)
		self.buttonSessionPause.clicked.connect(self.session.PauseSession)
		self.buttonSessionBlacklist.clicked.connect(self.session.BlacklistImage)
		self.buttonSessionEndExit.clicked.connect(self.session.EndSession)

		# mute share
		self.checkSettingsMute.stateChanged.connect(self.session.MuteFromSettings)
		self.toggleSessionMute.stateChanged.connect(self.session.MuteFromSession)
		
	def StartRefreshTimer(self):
		# print('start')
		self.needsrefresh = True
		self.refreshtimer.stop()
		self.refreshtimer.start(2000)

	def EndRefreshTimer(self):
		if self.needsrefresh:
			# print('end')
			self.session.GetFileList()
			self.needsrefresh = False

	def SetupViewer(self):
		self.viewer = QtImageViewer()
		self.viewer.canPan = False
		self.viewer.canZoom = False
		self.viewer.aspectRatioMode = QtCore.Qt.KeepAspectRatio
		self.viewer.setRenderHint(QPainter.Antialiasing, True)
		self.viewer.setRenderHint(QPainter.SmoothPixmapTransform, True)
		self.viewer.setRenderHint(QPainter.HighQualityAntialiasing, True)
		self.viewer.setImage(self.blankImage)
		self.viewLayout.addWidget(self.viewer)

	def FormatTime(self, seconds):
		m = int(seconds / 60)
		s = int(seconds % 60)
		mstring = str(m) + 'm' if m > 0 else ''
		spacer = ' ' if m > 0 and s > 0 else ''
		sstring = str(s) + 's' if s > 0 else ''
		f = mstring + spacer + sstring
		return f

	def Resource(self, filename):
		try:
			wd = sys._MEIPASS
		except AttributeError:
			wd = os.getcwd()
		file = os.path.join(wd, 'resources', filename)
		return file

if __name__ == "__main__":
	app = QApplication(sys.argv)
	dark_stylesheet = qdarkstyle.load_stylesheet_pyqt5()
	app.setStyleSheet(dark_stylesheet)
	window = TagRef()
	window.show()
	sys.exit(app.exec_())